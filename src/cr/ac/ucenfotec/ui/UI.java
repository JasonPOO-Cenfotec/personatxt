package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.CL;
import cr.ac.ucenfotec.bl.Persona;
import java.io.*;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static CL gestor = new CL();

    public static void main(String[] args) throws Exception {
        int opcion = -1; //variable que almacena la opción que selecciona el usuario del menú.
        do {
            out.println("**** Bienvenido al Sistema ***");
            out.println("1. Crear archivo.");
            out.println("2. Guardar una persona.");
            out.println("3. Listar Personas.");
            out.println("4. Buscar persona por cedula.");
            out.println("5. Salir.");
            out.print("Digite la opcion: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 5);
    }

    /*
      Función que se encarga de administrar la opción seleccionada por el usuario.
      Recibe como parámetro dicha oopción, y ejecuta las acciones necesarias según el valor del parámetro.
     */
    public static void procesarOpcion(int pOpcion) throws Exception {
        switch (pOpcion) {
            case 1:
                crearArchivo();
                break;
            case 2:
                guardarPersona();
                break;
            case 3:
                listarPersonas();
                break;
            case 4:
                buscarPersonaPorCedula();
                break;
            case 5://el usuario selecciona la opción de salir.
                out.println("Fin !!");
                break;
            default: // el usuario ingresa un valor que no está dentro de las opciones del menú
                out.println("Opcion inválida");
                break;
        }
    }

    public static void crearArchivo() throws Exception {
       // llamar a la CL para crear el archivo
        String respuesta = gestor.crearArchivo();
        System.out.println(respuesta);
    }

    public static void guardarPersona() throws Exception{
        out.print("Digite la cedula de la persona: ");
        int cedula = Integer.parseInt(in.readLine());
        out.print("Digite el nombre de la persona: ");
        String nombre = in.readLine();
        out.print("Digite la edad: ");
        int edad = Integer.parseInt(in.readLine());
        out.print("Digite la direccion: ");
        String direccion = in.readLine();

        // llamar a la CL y pasarle los datos para que guarde la persona
        String respuesta = gestor.guardarPersona(cedula,nombre,edad,direccion);
        System.out.println(respuesta);
    }

    public static void listarPersonas(){
        System.out.println("***** Lista de personas registradas *** ");
        for (String personaTemp:gestor.listarPersonas()) {
            System.out.println(personaTemp);
        }
    }

    public static void buscarPersonaPorCedula(){
    }
}
