package cr.ac.ucenfotec.bl;

import java.io.*;
import java.util.ArrayList;

public class CL {

    private final String NOMBRE_ARCHIVO = "PERSONA.txt";

    public String crearArchivo() throws Exception {
        File f = new File(NOMBRE_ARCHIVO);
        if (f.createNewFile()) {
            return "Archivo creado exitosamente!";
        } else {
            return "El archivo ya existe!.";
        }
    }

    public String guardarPersona(int cedula, String nombre, int edad, String direccion){

        Persona persona = new Persona(cedula,nombre,edad,direccion);

        try{
            FileWriter writer = new FileWriter(NOMBRE_ARCHIVO,true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(persona.toStringCSV());
            buffer.newLine();
            buffer.close();
            return "La persona fue registrada de manera exitosa!";
        }catch(Exception e){
            return "Se presentó un error al registrar la persona: " + e.getMessage();
        }
    }

    public ArrayList<String> listarPersonas(){
        ArrayList<String> lista = new ArrayList<>();
        String registro;
        try{
            //1. Abrir el achivo
            FileReader reader = new FileReader(NOMBRE_ARCHIVO);
            BufferedReader buffer = new BufferedReader(reader);

            //2. Leer el contenido del archivo
            while((registro = buffer.readLine()) != null) {
                //3. Crear los objetos de tipo Persona
                String[] datosPersona = registro.split(",");
                Persona persona = new Persona(Integer.parseInt(datosPersona[0]),//cedula
                                              datosPersona[1],                  //nombre
                                              Integer.parseInt(datosPersona[2]),//edad
                                              datosPersona[3]);                 //direccion
                //4. Agregar a la lista cada toString del objeto
                lista.add(persona.toString());
            }
            //5. devolver el arreglo
            return lista;

        }catch(Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
